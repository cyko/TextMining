Trabajo final contiene el cógigo python generado para el trabajo final de Text Mining.
El presente documento es una orientación sobre las diferentes tareas realizadas en el trabajo y cómo encontrarlo en el código.

1. Precondición: tener descargados los títulos de IMDB.
2. Descarga desde openSubtitles.org los subtítulos en inglés de las diferentes películas. 
    3.  download_subtitles.ipynb -> contiene la conexión con opensubtitles.org para la descarga de los subtitulos
3. Limpieza del texto.
    4.  clean_text.ipynb -> contiene la limpieza del texto, en cuestiones de formato, fechas, publicidades y demás, arma un dataframe con los datos de la BD de IMBD
4. Generación del pipeline.
    5.  pipeline.ipynb -> contiene la creación del pipeline que luego se usa en los clasificadores de RF y LGBM. Hace uso de los siguientes archivos:
        6. preprocess.ipynb, pipeline_class.ipynb
5. Breve análisis descriptivo de las variables.
    6. (Lo de Miguel)
7. Comparación del tiempo de ejecución del pipeline con los diferentes pasos
    8. grafico_tiempos_preproc_W2V.ipynb (Verificar si está OK -> Tincho??) 
6. Generación de modelos de clasificación
    7. Modelo simple bayesiano. 
        8. (Myriam)
    8.  Modelo random forest con optimización bayesiana.
        9. random_forest_clasifier.ipynb  -> optimizador del RF + ejecución de RF con los parámetros obtenidos + gráfica de los resultados  
    10. Modelo lightgbm con optimización bayesiana.
        11. (Tincho)  Tener en cuenta el metodo dataset_pipeline() del archivo pipeline.ipynb, que se le puede enviar los valores de obtenido del optimizador)