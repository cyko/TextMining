# -*- coding: utf-8 -*-
import urllib.request
import re


def gets_html_wikipedia(url):
    """ Lee un url y devuelve el código html """
    req = urllib.request.Request(url,
                                 headers = {'User-Agent':
                                            'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'})
    
    return(urllib.request.urlopen(req).read().decode("utf-8"))


def gets_n_wiki_links(html, n_links=3):
    """ Devuelve los primeros n links de un artículo, esta función es
    mala. La clase que viene vamos a ver cómo se mejoraría """
    links = re.compile('<a href="/wiki\S*" title.*?>', re.U).findall(html)
    out = []
    while links and len(out) < n_links:
        l = links.pop(0)
        if l.count("=") == 2 and ":" not in l:
            out.append("http://es.wikipedia.org" + re.compile('href="(.+?)"').findall(l)[0])

    return(out)


def scraper(to_scrap, n_links, max_depth):
    html_data = {}

    while to_scrap:
        url, depth = to_scrap.pop(0)
        print(url, depth)
        html_data[url] = gets_html_wikipedia(url)
        html_urls = gets_n_wiki_links(html_data[url], n_links=4)
        html_urls = [e for e in html_urls if e not in html_data]
        if depth < max_depth:
            to_scrap.extend([(e, depth + 1) for e in html_urls])

    return(html_data)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

to_scrap = [("http://es.wikipedia.org/wiki/Uruguay", 0),
            ("http://es.wikipedia.org/wiki/Pangea", 0)]  # Lista de seeds y páginas por scrapear

print(to_scrap[0][0])
print(gets_html_wikipedia(to_scrap[0][0]))
print(gets_n_wiki_links(gets_html_wikipedia(to_scrap[0][0]), n_links=2))

html_data = scraper(to_scrap, 4, 2)
