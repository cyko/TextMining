#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import urllib.request

url = "http://www.lanacion.com.ar/1729342"

url_handler = urllib.request.urlopen(url)

url_handler
type(url_handler)
dir(url_handler)
url_handler.info()
url_handler.geturl()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

raw = url_handler.read()

print(raw)
print(type(raw))
raw = raw.decode("utf-8")
print(type(raw))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
headers = {'User-Agent': user_agent}

req = urllib.request.Request(url, headers=headers)

with urllib.request.urlopen(req) as response:
   raw = response.read()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

import urllib.parse

urllib.parse.quote('/El Niño/')

urllib.parse.quote('https://docs.python.org/El Niño/', safe=':/')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

from bs4 import BeautifulSoup

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

url = "http://www.lanacion.com.ar/1729342"
with urllib.request.urlopen(url) as response:
    raw = response.read().decode("utf-8")

soup = BeautifulSoup(raw)
print(soup)
print(type(soup))
print(dir(soup))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

print(soup.title)
print(soup.a)
print(soup.figure)
print(soup.a.div)
print(type(soup.title))
print(dir(soup.title))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

print(soup.find_all('a'))
print(soup.find_all('img'))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

elemento = soup.find_all('a')[30]
print(type(elemento))
print(elemento.name)
print(elemento.string)
print(elemento.attrs)
print(type(elemento.attrs))
print(elemento["href"])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

print(elemento.string)
print(type(elemento.string))
print(str(elemento.string))
print(type(str(elemento.string)))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

print(soup.find_all(id = "pie"))
print(soup.find_all(id = "pie-facebook", class_ ="icon-facebook"))
print(soup.find_all(class_ = "icon-facebook"))
print(soup.find_all("a", class_ = "icon-facebook"))
print(soup.find_all("button", class_ = "icon-facebook"))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

cuerpo = soup.find_all("section", id = "cuerpo")[0]
texto_art = cuerpo.find_all("p")

for p in texto_art:
    print(p.find_all("a", class_ = "link"))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def quita_links_lanacion(soup):
    cuerpo = soup.find_all("section", id = "cuerpo")[0]
    texto_art = cuerpo.find_all("p")
    out = []
    for p in texto_art:
        for l in p.find_all("a", class_ = "link"):
            out.append(l["href"])
    return(out)

quita_links_lanacion(soup)
