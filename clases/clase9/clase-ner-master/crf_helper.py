import numpy as np
import pycrfsuite


def format_for_crfsuite(df, y_column):
    X_crf = []
    for idx in range(len(df)):
        X_crf.append(features_for_crf(idx, df))

    y_cfr = np.array([str(x) for x in df[y_column]])

    return X_crf, y_cfr


def features_for_crf(idx, df):
    # Formatea los features para CRFSuite. 
    key_value_pairs = []
    row = df.iloc[idx]

    banned_features = ["archivo", "desde", "hasta", "is_entity", "entity_tag", "freq_rae", "word_position"]

    key_value_pairs = ["{}={}".format(k, v) for (k, v) in row.items() if k not in banned_features]

    if row["word_position"] == 0:
        key_value_pairs.append('BOS') # Marca que es principio de secuencia (Begining of Sequence)
    else:
        prev_row = df.iloc[idx - 1]
        key_value_pairs.extend(["-1:{}={}".format(k, v) for (k, v) in prev_row.items() if k not in banned_features])

    if row["word_position"] == df[df.archivo == row["archivo"]].word_position.max():
        key_value_pairs.append("EOS")
    else:
        next_row = df.iloc[idx + 1]
        key_value_pairs.extend(["+1:{}={}".format(k, v) for (k, v) in next_row.items() if k not in banned_features])

    return key_value_pairs


def build_tagger(X_crf, y_crf):
    trainer = pycrfsuite.Trainer(verbose=False)
    trainer.append(X_crf, y_crf)

    trainer.set_params({
        'max_iterations': 500,  # stop earlier

        # Incluir transiciones no observadas (pero posibles)
        'feature.possible_transitions': True
    })

    trainer.train('TASS')
    trainer.logparser.last_iteration

    tagger = pycrfsuite.Tagger()
    tagger.open('TASS')

    return tagger
