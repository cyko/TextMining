import nltk

from nltk.tag.stanford import StanfordPOSTagger as POS_Tag
_path_to_model = "/home/pbrusco/apps/stanford-postagger-full-2018-02-27/models/spanish.tagger"
_path_to_jar = "/home/pbrusco/apps/stanford-postagger-full-2018-02-27/stanford-postagger.jar"


nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')
nltk.download('tagsets')


def simplify_pos_tag(tag):
    # https://nlp.stanford.edu/software/spanish-faq.shtml

    for key, val in TRADUCCIONES_POS_TAG.items():
        if tag.startswith(key):
            return val
    return tag


POS_TAGGER = POS_Tag(model_filename=_path_to_model, path_to_jar=_path_to_jar)

TRADUCCIONES_POS_TAG = dict(ao="AdjetivoOrdinal",
                            aq="AdjetivoDescriptivo",
                            cc="ConjuncionCoordinativa",
                            cs="ConjuncionSubordinante",
                            da="Articulo",
                            dd="Demostrativo",
                            de="Exclamativo",
                            di="ArticuloIndefinido",
                            dn="Numeral",
                            do="NumeralOrdinal",
                            dp="Posesivo",
                            dt="Interrogativo",
                            f="PuntuacionOtros",
                            i="PuntuacionOtros",
                            nc00="SustantivoComun",
                            nc0n="SustantivoComunInvariante",
                            nc0p="SustantivoComunPlural",
                            nc0s="SustantivoComunSingular",
                            np="SustantivoPropio",
                            p0="PronombreImpresonal",
                            pd="PronombreDemostrativo",
                            pe="PronombreExclamativo",
                            pi="PronombreIndefinido",
                            pn="PronombreNumeral",
                            pp="PronombrePersonal",
                            pr="PronombreRelativo",
                            pt="PronombreInterrogativo",
                            px="PronombrePosesivo",
                            rg="AdvervioGeneral",
                            rn="AdvervioNegador",
                            sp="Preposicion",
                            vag="VerboAuxiliarGerundio",
                            vai="VerboAuxiliarIndicativo",
                            vam="VerboAuxiliarImperativo",
                            van="VerboAuxiliarInfinitivo",
                            vap="VerboAuxiliarParticipio",
                            vas="VerboAuxiliarSubjuntivo",
                            vmg="VerboPrincipalGerundundio",
                            vmi="VerboPrincipalIndicativo",
                            vmm="VerboPrincipalImperativo",
                            vm0="VerboPrincipalImperativo",
                            vmn="VerboPrincipalInfinitivo",
                            vmp="VerboPrincipalParticipio",
                            vms="VerboPrincipalSubjuntivo",
                            vsg="VerboSemiAuxiliarGerundio",
                            vsi="VerboSemiAuxiliarIndicativo",
                            vsm="VerboSemiAuxiliarImperativo",
                            vsn="VerboSemiAuxiliarInfinitivo",
                            vsp="VerboSemiAuxiliarParticipio",
                            vss="VerboSemiAuxiliarSubjuntivo",
                            w="Fecha",
                            z="Numeral",
                            )
