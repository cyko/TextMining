## Ejercicio 1:
## Ejercicio 2:
## Ejercicio 3:

X_crf, y_crf = format_for_crfsuite(df_entities,  y_column="entity_tag")

trainer = pycrfsuite.Trainer(verbose=False)
trainer.append(X_crf, y_crf)

trainer.set_params({
#     'c1': 1.0,   # coefficient for L1 penalty
#     'c2': 1e-3,  # coefficient for L2 penalty
    'max_iterations': 500,  # stop earlier

    # include transitions that are possible, but not observed
    'feature.possible_transitions': True
})

trainer.train('TASS')
trainer.logparser.last_iteration

import sklearn.metrics
tagger = pycrfsuite.Tagger()
tagger.open('TASS')

display(sklearn.metrics.confusion_matrix(tagger.tag(X_crf), y_crf))
print("ACC: ", round(sklearn.metrics.accuracy_score(tagger.tag(X_crf), y_crf), 2))

from collections import Counter
info = tagger.info()

def print_transitions(trans_features):
    for (label_from, label_to), weight in trans_features:
        print("%-6s -> %-7s %0.6f" % (label_from, label_to, weight))

print("Transiciones")
print_transitions(Counter(info.transitions).most_common())


def  print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("peso=%0.6f etiqueta=%-6s %s" % (weight, label, attr))

print("Top features:")
print_state_features(Counter(info.state_features).most_common(40))
print()
print("Bottom features:")
print_state_features(Counter(info.state_features).most_common()[-20:])
