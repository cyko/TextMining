from IPython.display import display
from collections import defaultdict
from nltk.tokenize import WordPunctTokenizer
import pos_tag_helper
import pandas as pd
import glob
import os.path
import numpy as np


def crear_datos_de_entrenamiento():
    rae_frequencies = RAE_freqs()

    df = pd.DataFrame()
    for files in glob.glob("./data/training/input/*.txt"):
        archivo = os.path.basename(files).replace("input_", "").replace(".txt", "")
        print("creando datos para {}".format(archivo))

        info_archivo = create_words_info(archivo)
        info_archivo = add_pos_tags(info_archivo)

        info_archivo = add_ground_truth(info_archivo, archivo)

        df_archivo = pd.DataFrame(list(info_archivo.values()))
        df_archivo["archivo"] = archivo
        df_archivo["freq_rae"] = df_archivo.apply(lambda x: rae_frequencies[x.palabra.lower()], axis=1)

        df = df.append(df_archivo)

    df = df.set_index(np.arange(len(df)))
    df.to_csv("training_data_with_pos.csv", index=False)


def crear_datos_de_evaluacion():
    rae_frequencies = RAE_freqs()

    df_eval = pd.DataFrame()
    archivo = os.path.basename("./data/evaluation/input/input_evaluation.txt").replace("input_", "").replace(".txt", "")
    print("creando datos para {}".format(archivo))

    info_archivo = create_words_info(archivo, evaluation=True)
    info_archivo = add_pos_tags(info_archivo)

    info_archivo = add_ground_truth(info_archivo, archivo, evaluation=True)

    df_eval = pd.DataFrame(list(info_archivo.values()))
    df_eval["archivo"] = archivo
    df_eval["freq_rae"] = df_eval.apply(lambda x: rae_frequencies[x.palabra.lower()], axis=1)

    df_eval = df_eval.set_index(np.arange(len(df_eval)))
    df_eval.to_csv("evaluation_data_with_pos.csv", index=False)


def create_words_info(archivo, evaluation=False):
    if evaluation:
        set = "evaluation"
    else:
        set = "training"

    text = open("./data/{}/input/input_{}.txt".format(set, archivo)).read()

    words = WordPunctTokenizer().tokenize(text)
    spans = WordPunctTokenizer().span_tokenize(text)

    info = {}
    for i, (word, (desde, hasta)) in enumerate(zip(words, spans)):
        info[i] = dict(palabra=word, desde=desde, hasta=hasta, archivo=archivo, is_entity=False, word_position=i)

    return info


def add_ground_truth(info, archivo, evaluation=False):
    # Ground_truth contendrá todos los intervalos definidos en los archivos de output,
    # nosotros sólo tomaremos las palabras simples (1 palabra), pero se podría agregar al dataframe
    # las palabras compuestas del ground_truth si se quisiera.
    if evaluation:
        set = "evaluation"
    else:
        set = "training"

    output_a = open("./data/{}/gold/output_A_{}.txt".format(set, archivo)).read().strip().split("\n")
    output_b = open("./data/{}/gold/output_B_{}.txt".format(set, archivo)).read().strip().split("\n")
    text = open("./data/{}/input/input_{}.txt".format(set, archivo)).read()

    tags = dict([x.split('\t') for x in output_b])

    ground_truth = {}
    for l in output_a:
        ix, desde, hasta = l.split('\t')
        desde = int(desde)
        hasta = int(hasta)
        ground_truth[(desde, hasta)] = dict(desde=desde, hasta=hasta, palabra=text[desde:hasta], entity_tag=tags[ix])

    # Lo agregamos a la base
    for i, row in info.items():
        desde, hasta = row["desde"], row["hasta"]
        if (desde, hasta) in ground_truth:
            word_gt = ground_truth[(desde, hasta)]["palabra"]
            word_info = row["palabra"]
            assert word_gt == word_info, "no coincide {} vs {}".format(word_gt, word_info)
            row["entity_tag"] = ground_truth[(desde, hasta)]["entity_tag"]
            row["is_entity"] = True

    return info


def add_pos_tags(info):
    words = [i["palabra"] for i in info.values()]
    pos_tags = [pos_tag_helper.simplify_pos_tag(tag) for (w, tag) in pos_tag_helper.POS_TAGGER.tag(words)]

    for i, word in enumerate(words):
        assert info[i]["palabra"] == word, "{} y {}".format(info[i]["palabra"], word)
        info[i]["pos_tag"] = pos_tags[i]

    return info


def RAE_freqs(fname="CREA_total.TXT"):
    # CREA_freqs: http://corpus.rae.es/lfrecuencias.html (lista total de frecuencias)
    freq_rae = defaultdict(float)
    for i, l in enumerate(open(fname, encoding="Windows-1252").readlines()):
        if i > 0:
            _, word, _, freq = l.split("\t")
            word = word.strip()
            freq = float(freq)
            freq_rae[word] = freq
    return freq_rae


def display_with_style(df):
    display(df.style.apply(lambda x: ['background: orange   ' if x.name == "entity_tag" and x[i] == "Concept" else '' for i, _ in x.iteritems()])
                    .apply(lambda x: ['background: lightblue' if x.name == "entity_tag" and x[i] == "Action" else '' for i, _ in x.iteritems()])
                    .apply(lambda x: ['background: lightgreen' if x.name == "is_entity" and x[i] else '' for i, _ in x.iteritems()]))
