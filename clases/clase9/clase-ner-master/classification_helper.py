from sklearn.model_selection import cross_val_score
from sklearn.dummy import DummyClassifier
import sklearn.metrics

import numpy as np


def test(clf, X_train, y_train, X_test, y_test):
#     res_cv = cross_val_score(clf, X_train, y_train, cv=5, scoring="accuracy")
#     dummy_clf = DummyClassifier(strategy="most_frequent")
#     random_cv = cross_val_score(dummy_clf, X_train, y_train, cv=5, scoring="accuracy")

#     print("-----------------")
#     print("Clasificador: {}".format(type(clf)))
#     print("Resultados cross-validation: {} ± {} (random={})".format(round(res_cv.mean(), 3), round(res_cv.std(), 3), round(random_cv.mean(), 2)))
    dummy_clf = DummyClassifier(strategy="most_frequent")

    clf.fit(X_train, y_train)
    dummy_clf.fit(X_train, y_train)

    res_train = clf.score(X_train, y_train)
    random_train = dummy_clf.score(X_train, y_train)
    print("Sobre datos de entrenamiento: accuracy={} (random={})".format(round(res_train, 3), round(random_train, 2)))

    res_eval = clf.score(X_test, y_test)
    random_eval = dummy_clf.score(X_test, y_test)

    print("Resultados evaluación: accuracy={} (random={})".format(round(res_eval, 3), round(random_eval, 2)))
    print("-----------------")


class VerboClassifier:
    def fit(self, X, y):
        return self

    def predict(self, X):
        columnas = np.array([col for col in X.columns if ("Verbo" in col)])
        res = np.array(X[columnas].apply(lambda x: "Action" if np.any(x) else "Concept", axis=1))
        return res

    def get_params(self, deep):
        return {}

    def score(self, X_test, y_test):
        predicted = self.predict(X_test)
        return sklearn.metrics.accuracy_score(predicted, y_test)
